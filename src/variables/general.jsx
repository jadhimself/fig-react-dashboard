import React from "react";
import {
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from "reactstrap";

import taskImage1 from "assets/img/ryan.jpg";
import taskImage2 from "assets/img/eva.jpg";

// ##############################
// // // tasks list for Tasks card in Dashboard view
// #############################

const tasks = [
    {
        checked: true,
        image: taskImage1,
        text: 'Sign contract for "What are conference organizers afraid of?"'
    },
    {
        checked: false,
        image: taskImage1,
        text: "Lines From Great Russian Literature? Or E-mails From My Boss?"
    },
    {
        checked: true,
        image: taskImage2,
        text:
            "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit"
    }
];

// ##############################
// // // table head data and table body data for Tables view
// #############################

const thead = ["Name", "Country", "City", "Salary"];
const tbody = [
    {
        className: "table-success",
        data: ["Dakota Rice", "Niger", "Oud-Turnhout", "$36,738"]
    },
    {
        className: "",
        data: ["Minerva Hooper", "Curaçao", "Sinaai-Waas", "$23,789"]
    },
    {
        className: "table-info",
        data: ["Sage Rodriguez", "Netherlands", "Baileux", "$56,142"]
    },
    {
        className: "",
        data: ["Philip Chaney", "Korea, South", "Overland Park", "$38,735"]
    },
    {
        className: "table-danger",
        data: ["Doris Greene", "Malawi", "Feldkirchen in Kärnten", "$63,542"]
    },
    { className: "", data: ["Mason Porter", "Chile", "Gloucester", "$78,615"] },
    {
        className: "table-warning",
        data: ["Jon Porter", "Portugal", "Gloucester", "$98,615"]
    }
];

// ##############################
// // // stories for Timeline view
// #############################

const stories = [
    {
        // First story
        inverted: true,
        badgeColor: "danger",
        badgeIcon: "now-ui-icons business_briefcase-24",
        title: "Some Title",
        titleColor: "danger",
        body: (
            <p>
                Wifey made the best Father's Day meal ever. So thankful so happy so
                blessed. Thank you for making my family We just had fun with the
                “future” theme !!! It was a fun night all together ... The always rude
                Kanye Show at 2am Sold Out Famous viewing @ Figueroa and 12th in
                downtown.
            </p>
        ),
        footerTitle: "11 hours ago via Twitter"
    },
    {
        // Second story
        badgeColor: "success",
        badgeIcon: "now-ui-icons design-2_ruler-pencil",
        title: "Another One",
        titleColor: "success",
        body: (
            <p>
                Thank God for the support of my wife and real friends. I also wanted to
                point out that it’s the first album to go number 1 off of streaming!!! I
                love you Ellen and also my number one design rule of anything I do from
                shoes to music to homes is that Kim has to like it....
            </p>
        )
    },
    {
        // Third story
        inverted: true,
        badgeColor: "info",
        badgeIcon: "now-ui-icons gestures_tap-01",
        title: "Another Title",
        titleColor: "info",
        body: (
            <div>
                <p>
                    Called I Miss the Old Kanye That’s all it was Kanye And I love you
                    like Kanye loves Kanye Famous viewing @ Figueroa and 12th in downtown
                    LA 11:10PM
                </p>
                <p>
                    What if Kanye made a song about Kanye Royère doesn't make a Polar bear
                    bed but the Polar bear couch is my favorite piece of furniture we own
                    It wasn’t any Kanyes Set on his goals Kanye
                </p>
                <hr />
            </div>
        ),
        footer: (
            <UncontrolledButtonDropdown group={false}>
                <DropdownToggle caret className="btn-round" color="info">
                    <i className="now-ui-icons design_bullet-list-67" />{" "}
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
            </UncontrolledButtonDropdown>
        )
    },
    {
        // Fourth story
        badgeColor: "warning",
        badgeIcon: "now-ui-icons ui-1_send",
        title: "Another One",
        titleColor: "warning",
        body: (
            <p>
                Tune into Big Boy's 92.3 I'm about to play the first single from Cruel
                Winter also to Kim’s hair and makeup Lorraine jewelry and the whole
                style squad at Balmain and the Yeezy team. Thank you Anna for the invite
                thank you to the whole Vogue team
            </p>
        )
    }
];

// ##############################
// // // stories for Widgets view
// #############################

const widgetStories = [
    {
        // First story
        inverted: true,
        badgeColor: "danger",
        badgeIcon: "now-ui-icons business_briefcase-24",
        title: "Some Title",
        titleColor: "danger",
        body: (
            <p>
                Wifey made the best Father's Day meal ever. So thankful so happy so
                blessed. Thank you for making my family We just had fun with the
                “future” theme !!! It was a fun night all together ... The always rude
                Kanye Show at 2am Sold Out Famous viewing @ Figueroa and 12th in
                downtown.
            </p>
        ),
        footerTitle: "11 hours ago via Twitter"
    },
    {
        // Second story
        inverted: true,
        badgeColor: "success",
        badgeIcon: "now-ui-icons design-2_ruler-pencil",
        title: "Another One",
        titleColor: "success",
        body: (
            <p>
                Thank God for the support of my wife and real friends. I also wanted to
                point out that it’s the first album to go number 1 off of streaming!!! I
                love you Ellen and also my number one design rule of anything I do from
                shoes to music to homes is that Kim has to like it....
            </p>
        )
    },
    {
        // Third story
        inverted: true,
        badgeColor: "info",
        badgeIcon: "now-ui-icons gestures_tap-01",
        title: "Another Title",
        titleColor: "info",
        body: (
            <div>
                <p>
                    Called I Miss the Old Kanye That’s all it was Kanye And I love you
                    like Kanye loves Kanye Famous viewing @ Figueroa and 12th in downtown
                    LA 11:10PM
                </p>
                <p>
                    What if Kanye made a song about Kanye Royère doesn't make a Polar bear
                    bed but the Polar bear couch is my favorite piece of furniture we own
                    It wasn’t any Kanyes Set on his goals Kanye
                </p>
                <hr />
            </div>
        ),
        footer: (
            <UncontrolledButtonDropdown group={false}>
                <DropdownToggle caret className="btn-round" color="info">
                    <i className="now-ui-icons design_bullet-list-67" />
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
            </UncontrolledButtonDropdown>
        )
    }
];

// ##############################
// // // data for datatables.net in DataTables view
// #############################

const dataTable = {
    headerRow: ["Bot Name", "Type", "Office", "Age", "Actions"],
    footerRow: ["Bot Name", "Type", "Office", "Age", "Actions"],
    dataRows: [
        ["Adam", "Marketing Bot", "Edinburgh"],
        ["Lea", "Information Bot", "Tokyo"],
        ["Selena", "Pricing Bot", "San Francisco"]

    ]
};

// ##############################
// // // data for populating the calendar in Calendar view
// #############################

var today = new Date();
var y = today.getFullYear();
var m = today.getMonth();
var d = today.getDate();

const events = [
    {
        title: "All Day Event",
        allDay: true,
        start: new Date(y, m, 1),
        end: new Date(y, m, 1),
        color: "default"
    },
    {
        title: "Meeting",
        start: new Date(y, m, d - 1, 10, 30),
        end: new Date(y, m, d - 1, 11, 30),
        allDay: false,
        color: "green"
    },
    {
        title: "Lunch",
        start: new Date(y, m, d + 7, 12, 0),
        end: new Date(y, m, d + 7, 14, 0),
        allDay: false,
        color: "red"
    },
    {
        title: "Campaign Launch",
        start: new Date(y, m, d - 2),
        end: new Date(y, m, d - 2),
        allDay: true,
        color: "azure"
    },
    {
        title: "Networking Event",
        start: new Date(y, m, d + 1, 19, 0),
        end: new Date(y, m, d + 1, 22, 30),
        allDay: false,
        color: "azure"
    },

    {
        title: "API update",
        start: new Date(y, m, 21),
        end: new Date(y, m, 22),
        color: "orange"
    }
];

// ##############################
// // // for vector map row in Dashboard view
// #############################

const us_flag = require("../assets/img/flags/US.png");
const de_flag = require("../assets/img/flags/DE.png");
const au_flag = require("../assets/img/flags/AU.png");
const gb_flag = require("../assets/img/flags/GB.png");
const ro_flag = require("../assets/img/flags/RO.png");
const br_flag = require("../assets/img/flags/BR.png");
const websiteic = require("../assets/img/icons/website_icon.png")
const skypeic = require("../assets/img/icons/skype.png")
const androidic = require("../assets/img/icons/android.svg")
const iphoneic = require("../assets/img/icons/iphone.svg")
const slackic = require("../assets/img/icons/slack_icon.png")
const facebookic = require("../assets/img/icons/facebook.png")

const table_data = [
    { flag: us_flag, country: "USA", count: "2.920", percentage: "53.23%" },
    { flag: de_flag, country: "Germany", count: "1.300", percentage: "20.43%" },
    { flag: au_flag, country: "Australia", count: "760", percentage: "10.35%" },
    {
        flag: gb_flag,
        country: "United Kingdom",
        count: "690",
        percentage: "7.87%"
    },
    { flag: ro_flag, country: "Romania", count: "600", percentage: "5.94%" },
    { flag: br_flag, country: "Brasil", count: "550", percentage: "4.34%" }
];
const table_data2 = [
    { icon: facebookic,platform: "Facebook", count: "1,200", percentage: "36.9%" },
    { icon: websiteic,platform: "Website", count: "1000", percentage: "30%" },
    { icon: androidic,platform: "Android", count: "500", percentage: "15.3%" },
    {
        icon: iphoneic,
        platform: "IOS",
        count: "400",
        percentage: "12.3%"
    },
    { icon: skypeic,platform: "Twitter", count: "152", percentage: "4.9%%" },

];
const table_botdata = [
    { Intent: "Hello", type: "Greeting", percentage: "53.23%" },
    { Intent: "Product X", type: "Product Inquiry", percentage: "20.43%" },
    {  Intent: "Product Y", type: "Product Inquiry", percentage: "10.35%" },
    {  Intent: "Pricing", type: "Pricing Inquiry", percentage: "5.94%" },
    {  Intent: "About", type: "Info", percentage: "4.34%" }
];
const table_botdata2 = [
    { Time: "10AM-2PM", type: "Greeting", percentage: "53.23%" },
    { Time: "2PM-6PM", type: "Product Inquiry", percentage: "20.43%" },
    {  Time: "6PM-12AM", type: "Product Inquiry", percentage: "10.35%" },
    {  Time: "12AM-7AM", type: "Pricing Inquiry", percentage: "5.94%" },
    {  Time: "7AM-10AM", type: "Info", percentage: "4.34%" }
];
const table_botdata3 = [
    { Location: "Lebanon", type: "Greeting", percentage: "53.23%" },
    {Location: "UAE", type: "Product Inquiry", percentage: "20.43%" },
    {  Location: "Saudi Arabia", type: "Product Inquiry", percentage: "10.35%" },
    {  Location: "Jordan", type: "Pricing Inquiry", percentage: "5.94%" },
    {  Location: "Qatar", type: "Info", percentage: "4.34%" }
];

// tasks list for Tasks card in Dashboard view
// data for <thead> of table in Tables view
// data for <tbody> of table in Tables view
// data for the timeline in Timeline view
// data for the timeline in Widgets view
// data for datatables.net in DataTables view
// data for populating the calendar in Calendar view
// data for populating the table from Dashboard view
export {
    tasks,
    thead,
    tbody,
    stories,
    widgetStories,
    dataTable,
    events,
    table_data,
    table_data2,
    table_botdata,
    table_botdata2,
    table_botdata3
};
