import React from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  CardFooter,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
    Button,
} from "reactstrap";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";

import { PanelHeader, CardCategory, Stats } from "components";

import {

    chartsLine1,
    chartsLine1_facebook,
    chartsLine1_twitter,
    chartsLine1_mobile,
    chartsLine1_website,

    chartsLine2,
    chartsLine2_facebook,
    chartsLine2_twitter,
    chartsLine2_mobile,
    chartsLine2_website,

    chartsBar1,
    chartsBar1_facebook,
    chartsBar1_twitter,
    chartsBar1_mobile,
    chartsBar1_website,

    chartsBar2,
    chartsBar2_facebook,
    chartsBar2_twitter,
    chartsBar2_mobile,
    chartsBar2_website
} from "variables/charts";

var chartObject_1 = chartsLine1;
var chartObject_2 = chartsLine2;
var chartObject_3 = chartsBar1;
var chartObject_4 = chartsBar2;
var currentCat = "All";


class Charts extends React.Component {
  render() {
    return (
      <div>
        <PanelHeader
          content={
            <div className="header text-center">
              <h2 className="title">Analytics</h2>
              <p className="category">
                Get insight on your chatbot's performance, meaningful data is crucial.
              </p>
            </div>
          }
        />



        <div className="content">

            <div className="selectors">
                <Button color="primary" size="lg" onClick={ () => {
                    chartObject_1 = chartsLine1_facebook; //Chart 1
                    chartObject_2 = chartsLine2_facebook; //Chart 2
                    chartObject_3 = chartsBar1_facebook; //Chart 3
                    chartObject_4 = chartsBar2_facebook; //Chart 4
                    currentCat = "Facebook";
                    this.forceUpdate()

                }}>FACEBOOK</Button>

                <Button color="primary" size="lg" onClick={ () => {
                    chartObject_1 = chartsLine1_twitter; //Chart 1
                    chartObject_2 = chartsLine2_twitter; //Chart 2
                    chartObject_3 = chartsBar1_twitter; //Chart 3
                    chartObject_4 = chartsBar2_twitter; //Chart 4
                    currentCat = "Twitter";
                    this.forceUpdate()

                }}>TWITTER</Button>

                <Button color="primary" size="lg" onClick={ () => {
                    chartObject_1 = chartsLine1_mobile; //Chart 1
                    chartObject_2 = chartsLine2_mobile; //Chart 2
                    chartObject_3 = chartsBar1_mobile; //Chart 3
                    chartObject_4 = chartsBar2_mobile; //Chart 4
                    currentCat = "Mobile";
                    this.forceUpdate()

                }}>MOBILE</Button>

                <Button color="primary" size="lg" onClick={ () => {
                    chartObject_1 = chartsLine1_website; //Chart 1
                    chartObject_2 = chartsLine2_website; //Chart 2
                    chartObject_3 = chartsBar1_website; //Chart 3
                    chartObject_4 = chartsBar2_website; //Chart 4
                    currentCat = "Website";
                    this.forceUpdate()

                }}>WEBSITE</Button>

                <Button color="primary" size="lg" onClick={ () => {
                    chartObject_1 = chartsLine1; //Chart 1
                    chartObject_2 = chartsLine2; //Chart 2
                    chartObject_3 = chartsBar1; //Chart 3
                    chartObject_4 = chartsBar2; //Chart 4
                    currentCat = "All";
                    this.forceUpdate()

                }}>All Data</Button>
            </div>

          <Row>
            <Col xs={12} md={5} className="ml-auto">
              <Card className="card-chart">
                <CardHeader>
                  <CardCategory>{currentCat} Active Users</CardCategory>
                  <CardTitle tag="h4">{currentCat} Active Users</CardTitle>
                  {/*<UncontrolledDropdown>*/}
                    {/*<DropdownToggle*/}
                      {/*className="btn-round btn-simple btn-icon"*/}
                      {/*color="default"*/}
                    {/*>*/}
                      {/*<i className="now-ui-icons loader_gear" />*/}
                    {/*</DropdownToggle>*/}
                    {/*<DropdownMenu>*/}
                      {/*<DropdownItem>Action</DropdownItem>*/}
                      {/*<DropdownItem>Another Action</DropdownItem>*/}
                      {/*<DropdownItem>Something else here</DropdownItem>*/}
                      {/*<DropdownItem className="text-danger">*/}
                        {/*Remove data*/}
                      {/*</DropdownItem>*/}
                    {/*</DropdownMenu>*/}
                  {/*</UncontrolledDropdown>*/}
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data = {chartObject_1.data}
                      options = {chartObject_1.options}
                    />
                  </div>
                </CardBody>
                <CardFooter>
                  <Stats>
                    {[
                      {
                        i: "now-ui-icons arrows-1_refresh-69",
                        t: "Just Updated"
                      }
                    ]}
                  </Stats>
                </CardFooter>
              </Card>
            </Col>
            <Col xs={12} md={5} className="mr-auto">
              <Card className="card-chart">
                <CardHeader>
                  <CardCategory>{currentCat} Messages</CardCategory>
                  <CardTitle tag="h4">Number of Messages ({currentCat})</CardTitle>
                  {/*<UncontrolledDropdown>*/}
                    {/*<DropdownToggle*/}
                      {/*className="btn-round btn-simple btn-icon"*/}
                      {/*color="default"*/}
                    {/*>*/}
                      {/*<i className="now-ui-icons loader_gear" />*/}
                    {/*</DropdownToggle>*/}
                    {/*<DropdownMenu>*/}
                      {/*<DropdownItem>Action</DropdownItem>*/}
                      {/*<DropdownItem>Another Action</DropdownItem>*/}
                      {/*<DropdownItem>Something else here</DropdownItem>*/}
                      {/*<DropdownItem className="text-danger">*/}
                        {/*Remove data*/}
                      {/*</DropdownItem>*/}
                    {/*</DropdownMenu>*/}
                  {/*</UncontrolledDropdown>*/}
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartObject_2.data}
                      options={chartObject_2.options}
                    />
                  </div>
                </CardBody>
                <CardFooter>
                  <Stats>
                    {[
                      {
                        i: "now-ui-icons arrows-1_refresh-69",
                        t: "Just Updated"
                      }
                    ]}
                  </Stats>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={5} className="ml-auto">
              <Card className="card-chart">
                <CardHeader>
                  <CardCategory>{currentCat} Sentiment Analysis</CardCategory>
                  <CardTitle tag="h4">Number of Satisfied Clients ({currentCat})</CardTitle>
                </CardHeader>
                <CardBody>
                  <Bar data={chartObject_3.data} options={chartObject_3.options} />
                </CardBody>
                <CardFooter>
                  <Stats>
                    {[{ i: "now-ui-icons ui-2_time-alarm", t: "Last 7 days" }]}
                  </Stats>
                </CardFooter>
              </Card>
            </Col>
            <Col xs={12} md={5} className="mr-auto">
              <Card className="card-chart">
                <CardHeader>
                  <CardCategory>Cross Comparison</CardCategory>
                  <CardTitle tag="h4">Satisfied VS. Unsatisfied Users ({currentCat})</CardTitle>
                </CardHeader>
                <CardBody>
                  <Bar data={chartObject_4.data} options={chartObject_4.options} />
                </CardBody>
                <CardFooter>
                  <Stats>
                    {[{ i: "now-ui-icons ui-2_time-alarm", t: "Last 7 days" }]}
                  </Stats>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }


}

export default Charts;
