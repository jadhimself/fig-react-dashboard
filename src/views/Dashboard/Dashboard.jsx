import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Row,
    Col,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Table
} from "reactstrap";
// react plugin used to create charts
import { Line,Doughnut  } from "react-chartjs-2";
// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";
import { LoginPage} from  "../Pages/LoginPage.jsx";
import {
    PanelHeader,
    Stats,
    Statistics,
    CardCategory,
    Progress
} from "components";

import {
    dashboardPanelChart,
    dashboardActiveUsersChart,
    dashboardSummerChart,
    dashboardActiveCountriesCard
} from "variables/charts.jsx";

import jacket from "assets/img/saint-laurent.jpg";
import shirt from "assets/img/balmain.jpg";
import swim from "assets/img/prada.jpg";

import { table_data, table_data2 } from "variables/general.jsx";

var mapData = {
    AU: 760,
    BR: 550,
    CA: 120,
    DE: 1300,
    FR: 540,
    GB: 690,
    GE: 200,
    IN: 200,
    RO: 600,
    RU: 300,
    US: 2920
};
const ddata = {
    labels: [
        'Facebook',
        'Website',
        'Twitter'
    ],
    datasets: [{
        data: [2500, 2500, 300],
        backgroundColor: [
            '#000080',
            '#f9693a',

            '#2ca8ff'

        ],
        hoverBackgroundColor: [
            '#3a5896',
            '#e54d26',

            '#1da1f3'
        ]
    }]
};

class Dashboard extends React.Component {
    createTableData() {
        var tableRows = [];
        for (var i = 0; i < table_data.length; i++) {
            tableRows.push(
                <tr key={i}>
                    <td>
                        <div className="flag">
                            <img src={table_data[i].flag} alt="us_flag" />
                        </div>
                    </td>
                    <td>{table_data[i].country}</td>
                    <td className="text-right">{table_data[i].count}</td>
                    <td className="text-right">{table_data[i].percentage}</td>
                </tr>
            );
        }
        return tableRows;
    }

    createTableData2() {
        var tableRows = [];
        for (var i = 0; i < table_data2.length; i++) {
            tableRows.push(
                <tr key={i}>
                    <td>

                        <img src={table_data2[i].icon}  />

                    </td>
                    <td>{table_data2[i].platform}</td>
                    <td className="text-right">{table_data2[i].count}</td>
                    <td className="text-right">{table_data2[i].percentage}</td>
                </tr>
            );
        }
        return tableRows;
    }
    render() {
        return (
            <div>
                <PanelHeader
                    size="lg"
                    content={
                        <Line
                            data={dashboardPanelChart.data}
                            options={dashboardPanelChart.options}
                        />
                    }
                />
                <div className="content">
                    <Row>
                        <Col xs={12} md={12}>
                            <Card className="card-stats card-raised">
                                <CardBody>
                                    <Row>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="primary"
                                                icon="ui-2_chat-round"
                                                title="8500"
                                                subtitle="Total Messages"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="success"
                                                icon="ui-1_email-85"
                                                title={
                                                    <span>
                           800
                          </span>
                                                }
                                                subtitle="Total messages today"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="info"
                                                icon="users_single-02"
                                                title="5620"
                                                subtitle="Total Users"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="danger"
                                                icon="users_circle-08"
                                                title="353"
                                                subtitle="New Users"
                                            />
                                        </Col>

                                    </Row>
                                </CardBody>

                            </Card>
                        </Col>
                    </Row>
                    <Row>

                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Active Users</CardCategory>
                                    <CardTitle tag="h2">3,252</CardTitle>

                                </CardHeader>
                                <CardBody>
                                    <div className="chart-area">
                                        <Line
                                            data={dashboardActiveUsersChart.data}
                                            options={dashboardActiveUsersChart.options}
                                        />
                                    </div>
                                    <Table responsive>
                                        <tbody>{this.createTableData2()}</tbody>
                                    </Table>

                                </CardBody>
                                <CardFooter>
                                    <Stats>
                                        {[
                                            {
                                                i: "now-ui-icons arrows-1_refresh-69",
                                                t: "Just Updated"
                                            }
                                        ]}
                                    </Stats>
                                </CardFooter>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Summer Campaign</CardCategory>
                                    <CardTitle tag="h2">5,300</CardTitle>

                                </CardHeader>
                                <CardBody>
                                    <div className="chart-area">
                                        <Doughnut data={ddata} />
                                    </div>
                                    <h3 style={{color:'#ffffff'}}>Analytics: </h3>

                                    <div className="card-progress">
                                        <Progress badge="Delivery Rate" value="90" />
                                        <Progress color="success" badge="Open Rate" value="60" />
                                        <Progress color="info" badge="Click Rate" value="12" />
                                        <Progress color="primary" badge="Hard Bounce" value="5" />

                                        <Progress badge="Interest rate" value="40" />
                                        <Progress color="primary" badge="Engagement rate" value="60" />
                                    </div>
                                    <div className="card-progress">


                                    </div>
                                </CardBody>
                                <CardFooter>
                                    <Stats>
                                        {[
                                            {
                                                i: "now-ui-icons arrows-1_refresh-69",
                                                t: "Just Updated"
                                            }
                                        ]}
                                    </Stats>
                                </CardFooter>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="card-chart">
                                <CardHeader>
                                    <CardCategory>Active Countries</CardCategory>
                                    <CardTitle tag="h2">105</CardTitle>
                                </CardHeader>
                                <CardBody>

                                    <VectorMap
                                        map={"world_mill"}
                                        backgroundColor="transparent"
                                        zoomOnScroll={false}
                                        containerStyle={{
                                            width: "100%",
                                            height: "280px"
                                        }}
                                        containerClassName="map"
                                        regionStyle={{
                                            initial: {
                                                fill: "#e4e4e4",
                                                "fill-opacity": 0.9,
                                                stroke: "none",
                                                "stroke-width": 0,
                                                "stroke-opacity": 0
                                            }
                                        }}
                                        series={{
                                            regions: [
                                                {
                                                    values: mapData,
                                                    scale: ["#AAAAAA", "#444444"],
                                                    normalizeFunction: "polynomial"
                                                }
                                            ]
                                        }}
                                    />
                                    <Table responsive>
                                        <tbody>{this.createTableData()}</tbody>
                                    </Table>
                                </CardBody>
                                <CardFooter>
                                    <Stats>
                                        {[{ i: "now-ui-icons ui-2_time-alarm", t: "Last 7 days" }]}
                                    </Stats>
                                </CardFooter>
                            </Card>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} md={12}>
                            <Card className="card-stats card-raised">

                                <CardBody>
                                    <Row>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="primary"
                                                icon="ui-1_send"
                                                title="5000"
                                                subtitle="Total Sent Messages"
                                            />
                                        </Col>


                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="info"
                                                icon="emoticons_satisfied"
                                                title="1.042"
                                                subtitle="Average Conversation/User"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="primary"
                                                icon="ui-2_chat-round"
                                                title="3500"
                                                subtitle="Total Recieved Messages"
                                            />
                                        </Col>
                                        <Col xs={12} md={3}>
                                            <Statistics
                                                iconState="success"
                                                icon="tech_watch-time"
                                                title={
                                                    <span>
                       14 min
                          </span>
                                                }
                                                subtitle="Average Session Length"
                                            />
                                        </Col>



                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>

                </div>

            </div>
        );
    }
}

export default Dashboard;
